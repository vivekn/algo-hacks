#!/usr/bin/env python

def triplets(words):
    return [ (a, b, c) for a in words for b in words for c in words if len(set([a,b,c])) == 3]

def process(words):
    results = []
    for a, b, c in triplets(words):
        verticals = [ a[i] + b[i] + c[i] for i in range(3)]
        diags = [ a[0] + b[1] + c[2], a[2] + b[1] + c[0] ]
        if all( (x in words) for x in (verticals + diags)):
            results.append((a, b, c))
    return results

def main():
    n = int(raw_input())
    words = [raw_input() for i in range(n)]
    for a, b, c in process(words):
        print '%s\n%s\n%s\n\n' % (a,b,c) 

if __name__ == '__main__':
    main()    
