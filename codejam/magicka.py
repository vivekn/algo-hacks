def magic(combos, opposites, sequence):
    n = 0
    while n < (len(sequence)-1):
        if sequence[n:n+1] in combos:
            sequence = combos[sequence[n:n+1]] + sequence[2:]
        elif sequence[n:n+1] in opposites:
            sequence = sequence[(n+2):]
        else:
            n += 1

    return "[" + ", ".join(list(sequence)) + "]"

if __name__ == "__main__":
    f = open("bot.in")
    g = open("bot.o","w")
    for n,line in enumerate(f.readlines()[1:]):
        print line
        g.write("Case #%d: %d\n" %(n+1, bot(line[2:])))

