def dl(seq1, seq2, ti, td, tr, te):
    oneago = None
    thisrow = range(1, len(seq2) + 1) + [0]
    for x in xrange(len(seq1)):
        twoago, oneago, thisrow = oneago, thisrow, [0] * len(seq2) + [x + 1]
        for y in xrange(len(seq2)):
            delcost = oneago[y] + td
            addcost = thisrow[y - 1] + ti
            subcost = oneago[y - 1] + (seq1[x] != seq2[y])*tr
            thisrow[y] = min(delcost, addcost, subcost)
            if (x > 0 and y > 0 and seq1[x] == seq2[y - 1]
                and seq1[x-1] == seq2[y] and seq1[x] != seq2[y]):
                thisrow[y] = min(thisrow[y], twoago[y - 2] + te)
    return thisrow[len(seq2) - 1]

if __name__ == "__main__":
    [ti, td, tr, te] = map(int, raw_input().strip().split())
    s1 = raw_input().strip()
    s2 = raw_input().strip()
    print dl(s1, s2, ti, td, tr, te)
