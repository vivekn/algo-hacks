#Solution to Facebook's Small World puzzle
from functools import partial

def distance(user):
    [_, x, y] = map(float, user)
    return (x-(-180))**2 + (y-(-90))**2

def cartesian(a, b):
    [_, x, y] = map(float, a)
    [_, h, k] = map(float, b)
    return (x-h)**2 + (y-k)**2

def main():
    #f = open(raw_input())
    f = open('sw.input')
    users = [line.strip().split() for line in f]
    #users.sort(key = distance)
    results = {}
    for n, user in enumerate(users):
        #temp = sorted(users[max(0, n-3): n] + users[(n+1): (n+4)], key = partial(cartesian, user))[:3]
        temp = sorted(users, key = partial(cartesian, user))[1:4]
        results[int(user[0])] = [a[0] for a in temp]
    print results
    for i in range(1, len(results)+1):
        print i, ','.join(results[i])

if __name__ == '__main__':
    main()
