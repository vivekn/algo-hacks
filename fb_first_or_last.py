from fractions import *

def print_fraction(frac):
    return '%d/%d'% (frac.numerator,frac.denominator)
def n_tuples(a,n):
    '''generates a list of n-tuples from a continuous list'''
    if a==[]:
        return []
    head = tuple(a[:n])
    return [head] + n_tuples(a[n:],n)
    
        
def process_line(line):
    array = [int(x) for x in line.split()]
    b = n_tuples(array[2:],2)
    
    dic = {}
    c = sorted(b)
    c.reverse()
    for n,pair in enumerate(c):
        if n < array[0]-1:
            dic[pair] = 0
        else:
            dic[pair] = 1
    prob_crash = Fraction(0,1)
    runprob = Fraction(1,1)

    for pair in b:
        pcr = Fraction(1,pair[dic[pair]])
        prob_crash += runprob*pcr
        runprob *= Fraction(pair[dic[pair]]-1,pair[dic[pair]])
    return print_fraction(Fraction(1,1) - prob_crash) 
if __name__ == "__main__":
    print n_tuples([23,213,13,13],2)
    print process_line('2 2 118 80 400 316')
