def gcd(a,b):
        """ the euclidean algorithm """
        while a:
                a, b = b%a, a
        return b
    
def fib(n):
    a, b = 0, 1
    k = 0
    while k<n-1:
        a, b = b, a+b
        k+=1
    return b

def fun(n, m):
    return fib(gcd(n, m)) % 1000000007

print fun(192,156)


