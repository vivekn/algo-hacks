def is_perfect(n):
    return n**0.5 == int(n**0.5)

def fac(n):
    return reduce(lambda x, y: x*y, xrange(1,n+1))

ctr = 0
for x in xrange(1,int(fac(7)**.5) + 1):
    if is_perfect((x-1)**2 + 8 * fac(7)):
        ctr+=1
print ctr
