"""
The prime 41, can be written as the sum of six consecutive primes:

41 = 2 + 3 + 5 + 7 + 11 + 13
This is the longest sum of consecutive primes that adds to a prime below one-hundred.

The longest sum of consecutive primes below one-thousand that adds to a prime, contains 21 terms, and is equal to 953.

Which prime, below one-million, can be written as the sum of the most consecutive primes?


Sieve 1 million primes and start from 2 adding successive primes
"""
from bisect import bisect_left

def sieve(n):
    candidates = range(n+1)
    limit = int(n**.5) + 1

    for i in xrange(2, limit):
        if candidates[i]:
            candidates[2*i::i] = [None] * (n//i - 1)
    return [i for i in candidates[2:] if i]

#print len(sieve(1000000))


#LIMIT = 10000000
#primes = sieve(LIMIT)

def bsearch(array, key):
    """Performs an existence check on a sorted list using binary search"""
    i = bisect_left(array, key)
    if i!=len(array) and array[i] == key:
        return True
    return False

def get_max_poss():
    for i in range(1,len(primes)):
        if sum(primes[:i])>LIMIT:
            return i

def start_pos_bound(n):
    """
    Optimization to choose max start position, makes program 500 times faster.
    """
    part = []
    i = 0
    while sum(part) < LIMIT:
        part = primes[i:i+n]
        i += 1
    return i

def search():
    limit = get_max_poss()
    l = len(primes)
    for i in range(limit+1, 2, -1):
        bound = start_pos_bound(i)
        candidates = [sum(primes[n:n+i]) for n in xrange(bound)]
        candidates = [x for x in candidates if x in primes]
        if candidates:
            print max(candidates)
            return

#search()
