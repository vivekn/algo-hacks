class Node(object):
    def __init__(self, left = None, right = None, value = None):
        self.left = left
        self.right = right
        self.value = value
        self.discovered = False

q = []

def bfs_print(tree):
    global q
    print tree.value
    nub = lambda xs : [x for x in xs if x]
    q.extend(nub([tree.left, tree.right]))
     
    while len(q):
        x = q.pop(0)
        print x.value
        q.extend(nub([x.left, x.right]))

def dfs_print(tree):
    print tree.value
    ch = [tree.right, tree.left]
    remove_none(q)
    if ch == None:
        tree.processed = True
        return
    else:
        for x in ch:
            dfs_print(x)

def test():
    root = Node(value = 1)
    root.left = Node(value=2)
    root.right = Node(value=3)
    root.left.right = Node(value=5)
    root.right.right = Node(value=7)
    root.right.left = Node(value=6)
    root.left.left = Node(value=4)
    bfs_print(root)

if __name__ == '__main__':
    test()

        
