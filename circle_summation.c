/*
Circle summation problem on interviewstreet.com
*/
#include <stdio.h>
#include <stdlib.h>
#include <string.h> // For memcpy() function

typedef unsigned long u64;
void iterate(unsigned *, int, int, int);
void display_matrix(int **, int, int);

void vector_to_array(int **vector, int *array, int size) {
    int i;
    for (i = 0; i < size; i++) 
        array[i] = vector[i][0];
}

void array_to_vector(int *array, int **vector, int size) {
    int i;
    for (i = 0; i < size; i++) 
        vector[i][0] = array[i];
}

void display(unsigned int *array, int size) {
    int i;
    for(i = 0; i < size-1; i++) 
        printf("%u ", array[i]);
    printf("%u\n", array[i]);
}

void cyclic_copy(unsigned int *vector, unsigned int *source, int size, int zero_pos) {
    memcpy(vector, source + zero_pos, (size - zero_pos) * sizeof(int));
    memcpy(vector + (size - zero_pos), source, zero_pos * sizeof(int));
}

void recurrence(int **matrix, int k) {
    /* Generates a k*k matrix for the recurrence relation of the given problem.
       Assuming matrix to be an array of rows.
    */
    int i, j, *row = malloc(k * sizeof(int));
    /* initial setup */
    matrix[0][0] = 1;
    matrix[0][1] = 1;
    matrix[0][k-1] = 1;
    for (i = 2; i < k-1; i++)
        matrix[0][i] = 0;

    for(i = 1; i < k - 1; i++) {
        row = matrix[i];
        memcpy(row, matrix[i-1], k * sizeof(int));
        row[i%k] += 1;
        row[(i+1)%k] += 1;
    }
    for(i = 0; i < k; i++) 
        matrix[k-1][i] += matrix[0][i] + matrix[k-2][i];
    matrix[k-1][k-1] += 1;

}

void multiply(int **a, int **b, int **result, int l, int n) {
    /*  a is assumed to be an l*l square matrix
     *  b is an l*n matrix
     */
    int i, j, k;
    for (i = 0; i < l; i++)
        for (j = 0; j < n; j++) 
            result[i][j] = 0;

    for(i = 0; i < l; i++) 
        for (j = 0; j < n; j++) 
            for (k = 0; k < l; k++)
                result[i][j] =  (result[i][j] + ((a[i][k] * b[k][j]) % 1000000007)) % 1000000007;
}

int **exponentiate(int **matrix, int power, int k ) {
    int i, j, **x = (int **) calloc(k, k*sizeof(int)), **temp = (int **) calloc(k, k*sizeof(int));
    int **copy = (int **) calloc(k, k*sizeof(int));
    int **id_matrix = (int **) calloc(k, k*sizeof(int));
    // Memory allocation
    for(i = 0; i < k; i++) {
        x[i] = malloc(k * sizeof(int));
        temp[i] = malloc(k * sizeof(int));
        copy[i] = malloc(k * sizeof(int));
        memcpy(copy[i], matrix[i], k * sizeof(int));

        id_matrix[i] = malloc(k * sizeof(int));
        for (j = 0; j < k; j++) 
            id_matrix[i][j] = 0;
        id_matrix[i][i] = 1;
    }
    
    if (power == 0) {
        return id_matrix;
    }

    else if (power == 1)
        return matrix;

    else  {
        x = exponentiate(copy, (int) power/2, k);
        if (power % 2 == 0) {
           multiply(x, x, temp, k, k);
           return temp;
        }

        multiply(copy, x, temp, k, k);
        return temp;
    }
}

void fast_iterate(unsigned int *array, int n, int size) {
    int i, *copy = malloc(size * sizeof(int)), *copy2 = malloc(size * sizeof(int));
    int **temp = (int **) calloc(size, sizeof(int));
    int **vector = (int **) calloc(size, sizeof(int));
    int **rec_matrix = (int **) calloc(size, size * sizeof(int));
    // Memory allocation
    for(i = 0; i < size; i++) {
        rec_matrix[i] = malloc(size * sizeof(int));
        temp[i] = malloc(sizeof(int));
        vector[i] = malloc(sizeof(int));
    }

    recurrence(rec_matrix, size);
    rec_matrix = exponentiate(rec_matrix, (int) n/size, size);
    
    for (i=0; i < size; i++) {
        cyclic_copy(copy, array, size, i);
        array_to_vector(copy, vector, size);
        multiply(rec_matrix, vector, temp, size, 1);
        vector_to_array(temp, copy, size);
        display_matrix(vector, size, 1);
        display_matrix(rec_matrix, size, size);
        cyclic_copy(copy2, copy, size, size - i);
        iterate(copy2, n % size, size, i);
    }
    
}

void iterate(unsigned int *array, int n, int size, int offset) {
    int j, index;
    unsigned int *copy = malloc(size * sizeof(int));
    
    memcpy(copy, array, size * sizeof(int));

    for(j = 0; j < n; j++) {
        index = (offset + j) % size;

        if (index > 0 && index != size-1) 
            copy[index] = (copy[index] + ((copy[index - 1] + copy[index + 1]) % 1000000007)) % 1000000007;
        else if(index == size-1)
            copy[index] = (copy[index] + ((copy[index - 1] + copy[0]) % 1000000007)) % 1000000007;
        else
            copy[index] = (copy[index] + ((copy[size - 1] + copy[1]) % 1000000007)) % 1000000007;
    }
    display(copy, size);
}

    

void read_array(unsigned int *array, int size) {
    int i;
    for(i = 0; i < size; i++)
        scanf("%u", (array + i));
}

void display_matrix(int ** matrix, int x, int y) {
    int i;
    for (i = 0; i < x; i++)
        display(matrix[i], y);
}

main() {
    int num_cases, size, iterations;
    unsigned int *array; 
    scanf("%d", &num_cases);
    while (num_cases--) {
        scanf("%d %d", &size, &iterations);
        array = malloc(size * sizeof(int));
        read_array(array, size);
        fast_iterate(array, iterations, size);
        
        if (num_cases)
            printf("\n");
    }
}

        
