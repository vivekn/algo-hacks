from goldbach import check_prime

diag = 1
prime_ctr = 0

def new_add(n):
    global diag, prime_ctr
    if n%2:
        diag += 4
        l = [n*n - (n-1), n*n - 2*(n-1), n*n - 3*(n-1)]
        for x in l:
            if check_prime(x):
                prime_ctr += 1

def diagonal(n):
    ctr = i = 1
    while ctr> 0.1:
        i += 2
        new_add(i)
        ctr = (1.0 * prime_ctr)/diag
    print i


print diagonal(7)
     
