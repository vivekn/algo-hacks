def fac(n):
    if n in [0,1]:return 1
    return reduce(lambda x,y:x*y,range(1,n+1))

def Ch(n,r):
    if n <r: return 0
    return fac(n)/fac(r)/fac(n-r)
def mod(x,y):
    if x==1:return True
    return y%x!=0
def gcd(a,b):
        """ the euclidean algorithm """
        while a:
                a, b = b%a, a
        return b




def calc(a):
    N = a[0]
    A = a[1]
    B = a[2]
    C = a[3]
    D = a[4]
    req = ( Ch(N,2)*2 * sum([1+(N-2)**(y-x-1) for x in range(A,B+1) for y in range(C,D+1) if ((x<y) and mod(x,y))]))%(10**9+7)
    return req

if __name__ == "__main__":
    f = open('input.txt')
    for line in f.readlines()[1:]:
        print calc(map(int,line.split()))
    
              
              
