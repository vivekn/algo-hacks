"""
 It is possible to show that the square root of two can be expressed as an infinite continued fraction.

  2 = 1 + 1/(2 + 1/(2 + 1/(2 + ... ))) = 1.414213...

  By expanding this for the first four iterations, we get:

  1 + 1/2 = 3/2 = 1.5
  1 + 1/(2 + 1/2) = 7/5 = 1.4
  1 + 1/(2 + 1/(2 + 1/2)) = 17/12 = 1.41666...
  1 + 1/(2 + 1/(2 + 1/(2 + 1/2))) = 41/29 = 1.41379...

  The next three expansions are 99/70, 239/169, and 577/408, but the eighth expansion, 1393/985, is the first example where the number of digits in the numerator exceeds the number of digits in the denominator.

  In the first one-thousand expansions, how many fractions contain a numerator with more digits than denominator?


  ---------------------------------------------------------------------------------------
  Find a way to represent the continued fraction
  Then find a coprime pair of numerator and denominator

  found a recursive way to represent the continued fraction!
"""
from fractions import Fraction

memoise = {1: Fraction(1,2)}

def frac_part(n):
    """
    returns the fractional part of the series
    """
    if n in memoise:
        return memoise[n]
    memoise[n] = 1 / (2 + memoise[n-1])
    return memoise[n]

def continued(n):
    return 1 + frac_part(n)

def search():
    ctr = 0
    for n in range(1,1001):
        value = continued(n)
        p = value.numerator
        q = value.denominator
        if len(str(p))>len(str(q)):
            ctr+=1
    print ctr

if __name__ == "__main__":
    search()


