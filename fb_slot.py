import cPickle
from itertools import *
#Author: Vivek Narayanan
f = open('eta')
#f = open('eta','w')
#eta is a file generated using pickle module containing a list of 10 million possible starting values of secret
# code for eta
## cPickle.dump(map(nextn,range(10000001)),f)

biglist = cPickle.load(f)


inp = open('slot')
#outp = open('oslot','w')


def nextn(s):
    return (s*(5402147)+54321) % 10000001



s1 = 0
def nexts(i):
    return (s1*(5402147)+54321) % 10000001

def check(seq):
    y = ifilter(lambda n:n%1000==seq[0],biglist)
    result = []
    for x in seq[1:]:
        y = imap(nextn,y)
        y = ifilter(lambda n:n%1000==x,y)
    if y and len(list(y))==1:
        temp = y[-1]
        for i in range(10):
            temp = nextn(temp)
            result.append(int(temp%1000))
        print " ".join(map(str,result))
    elif len(list(y))>1:
        print "Not enough observations"
    else:
        print "Wrong machine"
    return

if __name__ == "__main__":
    
    for line in inp.readlines()[1:]:
        check(map(int,line.strip().split()[1:]))
