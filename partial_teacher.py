import sys

def process(inp, stream):
    for n, x in enumerate(stream[1:]):
        n += 1
        
        if x == "L" :
            inp[n] = inp[n-1] - 1
        elif x == "R" :
            inp[n] = inp[n-1] + 1
        elif x == "=":
            inp[n] = inp[n-1]
    return inp

def reduce_list(stream):
    k = 1
    if stream[0] == "L":
        k = 2
    inp = [k] * (len(stream) + 1)
    i = 1
    for x in range(len(stream)+1):
        inp = process(inp, stream)
        
    return " ".join(map(str,inp))
            
if __name__ == "__main__":
    x = int(raw_input())
    print reduce_list(list(raw_input().strip()[:(x-1)]))
