from fractions import *

def print_fraction(frac):
    return '%d/%d'% (frac.numerator,frac.denominator)
def process_line(line):
    array = [int(x) for x in line[2:].split()]
    b = []
    for i in range(0,len(array),2):
        b.append((array[i],array[i+1]))
    dic = {}
    c = sorted(b)
    c.reverse()
    for n,pair in enumerate(c):
        if n < c[0]-1:
            dic[pair] = 0
        else:
            dic[pair] = 1
    prob_crash = Fraction(0,1)
    runprob = Fraction(1,1)

    for pair in b:
        pcr = Fraction(1,pair[dic[pair]])
        prob_crash += runprob*pcr
        runprob *= Fraction(pair[dic[pair]]-1,pair[dic[pair]])
    return print_fraction(Fraction(1,1) - prob_crash) 
