A=B={}

a1=a2=a3=a4=a5=0
b1=b2=b3=b4=b5=0

def init_a(seq):
    A[1]=a1=seq[0]
    A[2]=a2=seq[1]
    a3 = seq[2]
    a4 = seq[3]
    a5 = seq[4]
def init_b(seq):

    B[1] =b1= seq[0]
    B[2] =b2= seq[1]
    b3 = seq[2]
    b4 = seq[3]
    b5 = seq[4]
    
def calc_a(n,p):
    if n ==1:return a1
    if n==2:return a2
    res = ((A[n-2]*a3+A[n-1]*a4+a5)%p)
    A[n]=res
    return res

def calc_b(n,p):
    if n ==1:return b1
    if n==2:return b2
    res = ((B[n-2]*b3+B[n-1]*b4+b5)%p)
    B[n] = res
    return res

if __name__ == "__main__":
    f = open('input.txt')
    lines = f.readlines()[1:]
    l = len(lines)
    for x in range(l//3):
        proc = lines[3*x:(3*x+3)]
        for n,line in enumerate(proc):
            proc[n]= [int(x) for x in line.split()]
        P = proc[0][0]
        L = proc[0][1]
        N = proc[1][0]
        M = proc[2][0]

        init_a(proc[1][1:])
        init_b(proc[2][1:])

        As = [calc_a(n,P) for n in xrange(1,M+1)]
        Bs = [calc_b(n,P) for n in xrange(1,N+1)]
        print As,Bs,A,B
        print len([(x,y) for x in As for y in Bs if ((x*y)%P)<L])
