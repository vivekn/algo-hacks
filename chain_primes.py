"""
The primes 3, 7, 109, and 673, are quite remarkable. By taking any two primes and concatenating them in any order the result will always be prime. For example, taking 7 and 109, both 7109 and 1097 are prime. The sum of these four primes, 792, represents the lowest sum for a set of four primes with this property.

Find the lowest sum for a set of five primes for which any two primes concatenate to produce another prime.
------------------------------------------------------------------------------------------------------------
Method: Take a sieve of 5 and 6 digit primes, split them up and see if they are composed of primes
Get a reduced list of primes
Now take a prime,
find the eleprimes it is composed of , and get a list of primes containing those eleprimes
match common - other_eleprimes

"""
from functools import partial
from primesums import sieve, bsearch, bisect_left

primes = sieve(10**8)
marked = set([])
cands =  set([])
cands2 = set([])
cands3 = set([])
cands4 = set([])
chk_prime = lambda x: bsearch(primes, x)

def split_prime(n):
    number = str(n)
    splits = [(number[:i], number[i:]) for i in range(1, len(number))]
    for x, y in splits:
        a, b = int(x), int(y)
        if chk_prime(a) and chk_prime(b):
            if chk_prime(int(y+x)):
                marked.append((n, (a, b)))
                return
def is_candidate(tup, n):
    """Checks if n can be added to the combination tup"""
    chk2 = lambda x: chk_prime(int(str(x) + str(n)))
    chk3 = lambda x: chk_prime(int(str(n) + str(x)))
    return all(map(chk3, tup)) and all(map(chk2, tup))

if __name__ == "__main__":
    prime_set = primes[:bisect_left(primes, 10000) + 1]
    prime_set2 = primes[:bisect_left(primes, 1000) + 1]
    for i in prime_set2:
        for j in prime_set:
            if i==j or (i in marked and j in marked):
                continue
            a, b = str(i), str(j)
            x, y = int(a+b), int(b+a)
            if chk_prime(x) and chk_prime(y):
                marked.add(i)
                marked.add(j)
                cands.add((min(i,j), max(i,j)))
    
    for tup in cands:
        poss = filter(partial(is_candidate, tup), prime_set2 )
        if len(poss):
            for n in poss:
                cands2.add(tuple(list(tup) + [n]))
        
    for tup in cands2:
        poss = filter(partial(is_candidate, tup), prime_set2 )
        if len(poss):
            for n in poss:
                cands3.add(tuple(list(tup) + [n]))

    for tup in cands3:
        poss = filter(partial(is_candidate, tup), prime_set )
        if len(poss):
            for n in poss:
                cands4.add(tuple(list(tup) + [n]))
    print cands4, min(cands4, key=sum)
    print is_candidate((3,7,109), 673), chk_prime(109673)



