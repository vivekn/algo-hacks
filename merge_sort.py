# Simple implementation of merge sort
import random

def combine(a, b):
    result = []
    while len(a) or len(b):
        if len(a) and len(b):
            if a[0] < b[0]:
                result.append(a.pop(0))
            else:
                result.append(b.pop(0))
        elif len(a):
            result.append(a.pop(0))
        else:
            result.append(b.pop(0))
    return result

def merge_sort(array):
    l = len(array)
    if l < 2:
        return array
    return combine ( merge_sort(array[:(l//2)]), merge_sort(array[(l//2):]) )

test = range(1000000)
random.shuffle(test)
print merge_sort(test)[636]
