# Subsets 

def process_bin(string, no_of_bits):
    prefix = (no_of_bits - len(string) + 2)*'0'
    return prefix+string[2:]

def get_subset(array, binary):
    return tuple(array[n] for n, char in enumerate(process_bin(binary, len(array))) if char == '1')

def subsets(array):
    length = len(array)
    return set(get_subset (array, bin(i)) for i in xrange(2**length))  

for sub in (range(2**8)):
    print sub, get_subset(range(8), bin(sub)), bin(sub)
