"""
It was proposed by Christian Goldbach that every odd composite number can be written as the sum of a prime and twice a square.

9 = 7 + 212
15 = 7 + 222
21 = 3 + 232
25 = 7 + 232
27 = 19 + 222
33 = 31 + 212

It turns out that the conjecture was false.

What is the smallest odd composite that cannot be written as the sum of a prime and twice a square?

Algo: Start from biggest possible square, check_prime the other number -> bruteforce n^2 algo with an optimization.
"""
import eventlet
eventlet.monkey_patch()

def check_prime(n):
    if (n < 2) or (n%2==0):
        return False
    for m in range(2, int(n**.5)+1):
        if n%m==0:
            return False
    return True

def is_goldbach(n):
    limit = int((n/2)**.5) 
    for i in range(limit, 0, -1):
        number = n - 2*i*i
        if check_prime(number):
            return True
    return False

def search(limit):
    for i in xrange(3, limit, 2):
        if not is_goldbach(i):
            if not check_prime(i):
                print i 
                return

def primes(a, b):
    return [x for x in xrange(a, b+1) if check_prime(x)]

def wrap(n):
    return (check_prime(n), n)

def parallel(a, b):
    pool = eventlet.GreenPool()
    results = pool.imap(wrap, xrange(a, b+1))
    return [result[1] for result in results if result[0]]   

print len (parallel(3, 100000))

#LIMIT = 198808091
#search(LIMIT)

