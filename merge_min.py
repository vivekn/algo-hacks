# Divide and conquer implementation of finding the minimum element 
import random
import time

def combine(a, b):
    return a if a > b else b

def _merge_min(array):
    l = len(array)
    if l == 1:
        return array[0]
    return combine ( _merge_min(array[:(l//2)]), _merge_min(array[(l//2):]) )

merge_min = lambda x: _merge_min(x)

def linear_min(array):
    large = array[0] 
    for x in array:
        if x > large:
            large = x
    return large

test = range(10000)
random.shuffle(test)
tic = time.clock()
print merge_min(test)
toc = time.clock()
print "merge_min Time: " + str(toc -tic) 
tic = time.clock()
print max(test)
toc = time.clock()
print "min Time: " + str(toc -tic) 
tic = time.clock()
print linear_min(test)
toc = time.clock()
print "linear_min Time: " + str(toc -tic) 
