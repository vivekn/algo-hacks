def lcount(seq, n):
    seq.reverse()
    count = 0
    for x in seq:
        if x <= n:
            count += 1
            n = x
        else:
            break
    return count

def rcount(seq, n):
    count = 0
    for x in seq:
        if x <= n:
            count += 1
            n = x
        else:
            break
    return count

def petya(seq):
    temp = []
    for n, x in enumerate(seq):
        temp.append(lcount(seq[:n], x) + rcount(seq[(n+1):], x) + 1)
    return max(temp)

if __name__ == "__main__":
    k = int(raw_input())
    print petya(map(int, raw_input().strip().split()))
    
