from pentagonal import is_int


def one_of(functions, args):
    return [f(args) for f in functions].count(True) >= 1

triangular = lambda n: is_int((-1 + (1 + 8*n)**.5)/2)
square = lambda n: is_int(n**.5)
pentagonal = lambda n: is_int((1 + (1 + 24*n)**.5)/6)
heptagonal = lambda n: is_int((3 + (9 + 40*n)**.5)/10)
hexagonal = lambda n: is_int((1 + (1 + 8*n)**.5)/4)
octagonal = lambda n: is_int((2 + (4 + 12*n)**.5)/6)

poly = [triangular, square, pentagonal, heptagonal, hexagonal, octagonal]

chains = set([])
chains2 = set([])
def recurse(targets, chains, chains2):
    if (not len(chains)) and (not len(chains2)):
        for a in range(10, 100):
            for b in range(10, 100):
                no = 100*a+b
                if one_of(targets, no):
                    chains.add((a,b))
        return chains, chains2
    chains2 = set([])
    for tup in chains:
        for x in range(10, 100):
            no = tup[-1]*100 + x
            if one_of(targets, no):
                chains2.add(tuple(list(tup)+[x]))

    return chains, chains2




if __name__ == '__main__':
    chains = set([])
    targets = set([triangular, square, pentagonal])

    chains, chains2 = recurse(targets, chains, chains2)
    chains, chains2 = recurse(targets, chains, chains2)

    chains = set([])
    for (a, b, c) in chains2:
        no = 100*c + a 
        flag = True
        if one_of(targets, no):
            for func in targets:
                if map(func, [a, b, c]).count(True) < 1:
                    flag = False
            if flag:
                chains2.add((a, b, c))
    
    print min(chains2, key=sum), chains2
    


    ## Find triangular chains
    #for a in range(11,100):
    #    for b in range(11, 100):
    #        no = 100*a + b
    #        if one_of(poly, triangular, no):
    #            chains.add((a,b))

    ## Find square chains
    #chains2 = set([])
    #for (a, b) in chains:
    #    for c in range(11,100):
    #        no = 100*b + c
    #        if one_of(poly, square, no):
    #            chains2.add((a, b, c))
   
    ## Find pentagonal chains
    #chains = set([])
    #for (a, b, c) in chains2:
    #    for d in range(11,100):
    #        no = intcat(c, d)
    #        if one_of(poly, pentagonal, no):
    #            chains.add(tuple(sorted((a, b, c, d))))
    #chains2 = set([])

    ## Find hexagonal chains
    #for (a, b, c, d) in chains:
    #    for e in range(11,100):
    #        no = intcat(d, e)
    #        if one_of(poly, hexagonal, no):
    #            chains2.add(tuple(sorted((a, b, c, d, e))))


    ## Find heptagonal chains
    #chains = set([])
    #for (a, b, c, d, e) in chains2:
    #    for f in range(11,100):
    #        no = intcat(e, f)
    #        if one_of(poly, heptagonal, no):
    #            chains.add(tuple(sorted((a, b, c, d, e, f))))
    #chains2 = set([])

    ## Find octagonal chains
    #for (a, b, c, d, e, f) in chains:
    #    no = intcat(f, a)
    #    if one_of(poly, octagonal, no):
    #        chains2.add(tuple(sorted((a, b, c, d, e, f))))


