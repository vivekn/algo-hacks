"""
a^3 + b^3 = c^3 + d^3
a,b,c,d distinct
"""

def ram(n):
    """ return a set of ram nos less than n """
    mapping = {}
    limit = int(n**(1.0/3.0)) + 1
    for a in xrange(1, limit+1):
        for b in xrange(1, limit+1):
            sum_ = a**3 + b**3
            if sum_ in mapping:
                mapping[sum_].update((a, b))
            else:
                mapping[sum_] =  set([a,b])
    return len ([tuple(v) for k, v in mapping.items() if len(v)==4])

print ram(10 ** 9) # Prints all Ramanujam numbers under 1 billion in 4s. O(n**0.66)
