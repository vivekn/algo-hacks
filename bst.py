"""
A proof of concept binary search tree.
"""

class BSTNode(object):
    def __init__(self, parent=None, left=None, right=None, value=None):
        self.parent = parent
        self.left = left
        self.right = right
        self.value = value

def search_tree(tree, value):
    if tree.value == None or tree == None:
        return False
    elif tree.value == value:
        return True
    elif value > tree.value:
        return search_tree(tree.right, value)
    else:
        return search_tree(tree.left, value)

def insert_tree(tree, value, parent):
    if tree = None:
        tree = BSTNode(parent = parent, value = value)
    if value > tree.value:
        insert_tree(tree.right, value, tree)
    else:
        insert_tree(tree.left, value, tree)



    
