
sequences = {"a":1,"b":1,"ab":3,"aa":2,"bb":2,"ba":3}

def remove(n,word):
    ## deletes n chars from a word
    splits = [(word[:i],word[i:]) for i in range(len(word)-n+1)]
    return filter(lambda s:len(s)==len(word)-n+1,list(([x+a+y[n:] for x,y in splits for a in set(y[:n]) ])))
              

def calc(seq):
    if seq in sequences:
        return sequences[seq]
    ctr = 1
    l = len(seq)
    temp_seq = []
    for j in range(2,l+1):
        temp_seq+=remove(j,seq)
        
    temp_seq = (temp_seq)
     
    ctr += sum(map(calc,temp_seq)) #+ len(temp_seq)
    sequences[seq] = ctr
    return ctr
if __name__ == "__main__":
    f = open("input.txt")
    for line in f.readlines()[1:]:
        print calc(line.strip())%(10**9+7)
