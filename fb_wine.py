# fb hacker cup wine tasting
import itertools as it

def fac(n):
    if n in [0,1]:return 1
    return reduce(lambda x,y:x*y,xrange(1,n+1))
def C(n,r):
    return fac(n)/fac(n-r)/fac(r)
def wine(g,c):
    wsum = 1
    ctr = g-2
    qsum = 0
    while ctr>=c:
        qsum = sum([fac(x) for x in range (1,g-ctr)])
        wsum += C(g,ctr) * (fac(g-ctr) - qsum)
        ctr-=1
    return wsum

if __name__ == "__main__":
    print wine(13,10)
