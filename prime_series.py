"""
By replacing the 1st digit of *3, it turns out that six of the nine possible values: 13, 23, 43, 53, 73, and 83, are all prime.

By replacing the 3rd and 4th digits of 56**3 with the same digit, this 5-digit number is the first example having seven primes among the ten generated numbers, yielding the family: 56003, 56113, 56333, 56443, 56663, 56773, and 56993. Consequently 56003, being the first member of this family, is the smallest prime with this property.

Find the smallest prime which, by replacing part of the number (not necessarily adjacent digits) with the same digit, is part of an eight prime value family.
"""

from primesums import sieve
from itertools import combinations

LIMIT = 1000000
primes = sieve(LIMIT)
history = {} # Avoid repeating patterns

def check_pattern(pattern, num):
    """
    Checks a number against a pattern like '56xx1'
    """
    if len(pattern) != len(str(num)):
        return False
    stack = []
    for a, b in zip(pattern, str(num)):
        if a == 'x':
            stack.append(b)
        elif a!=b:
            return False
    return stack == [x for x in stack if x == stack[0]]

def generate_patterns(prime):
    patterns = []
    length = len(str(prime))
    for n in range(2, length):
        for combo in combinations(range(length), n): 
            tempstr = list(str(prime))
            for index in combo:
                tempstr[index] = 'x'
            pattern = ''.join(tempstr)
            if pattern in history:
                continue
            else:
                patterns.append(pattern)
                history[pattern] = True 
    return patterns

def reduce_set(nums):
    return [x for x in nums if len(str(x)) >= (len(set(str(x))) + 3) ]



def search():
    answer = maxset = []

    for r in range(1, 6):
        prime_set = reduce_set([x for x in primes if (10**r < x < 10**(r+1))])
        for prime in prime_set:
            for pattern in generate_patterns(prime):
                answer = [prime for prime in prime_set if check_pattern(pattern, prime)]
                if len(answer)>len(maxset):
                    maxset = [x for x in answer]
                    print maxset
    print maxset

search()
print len(history) * len(primes)








