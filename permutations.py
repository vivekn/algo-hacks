
memory = {}

st = lambda x: ''.join(sorted(x))

def perm(string):
    if st(string) in memory:
        return memory[st(string)]

    if len(string) == 1:
        return set([string])
    perms = {} 
    rotations = [ string[i:] + string[:i] for i in range(len(string))]
    for word in rotations:
        char, new_str = word[0], word[1:]
        for combo in perm(new_str):
            temp_set = ([combo[:i] + char + combo[i:] for i in range(len(combo)+1)])
            for x in temp_set:
                perms[x] = 1

    memory[st(string)] = perms.keys()
    return perms.keys()

print len(perm("Gooooogle"))
