# Based on spoj problem - TRIP
memory = {}

def prune(xs):
    length = max( map (len, xs))
    return [x for x in xs if len(x) == length]

def prefix(pre, words):
    return [pre + word for word in words]

def splitat(word, char):
    return word[(word.find(char) + 1):]

def lcs(a, b):
    if (a,b) in memory:
        return memory[(a,b)]

    results = ['']
    if a and b:
        alphabet = set(a) & set(b)
        for char in alphabet:
            x, y = splitat(a, char), splitat(b, char)
            results.extend (prefix (char, lcs (x, y)))

    memory[(a,b)] = prune(results)
    memory[(b,a)] = memory[(a,b)]
    return memory[(a,b)]


def main():
    num_cases = int (raw_input())
    for i in range(num_cases):
        a, b = raw_input().strip(), raw_input().strip()
        for result in sorted (set(lcs(a, b))):
            print result
        if num_cases > 1:
            print


if __name__ == '__main__':
    main()
