from random import randint

def normal(a, b):
    return int(a/b)

def restricted(x, y):
    ctr = -1
    while x>0:
        x -= y
        ctr += 1
    return ctr

#Tests

def test(func, n=10000):
    for i in range(n):
        x, y = randint(1, 1000000), randint(1, 100)
        p = func(x, y)

test(normal, 100)

